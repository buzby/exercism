module Clock (addDelta, fromHourMin, toString) where

import Prelude hiding (min)

data Clock = Clock Int Int
  deriving Eq

instance Show Clock where
    show (Clock a b) = hour ++ ':' : min 
        where
            pad n
                | n < 10 = '0' : show n
                | otherwise = show n
            hour = pad a
            min = pad b

fromHourMin :: Int -> Int -> Clock
fromHourMin hour min = Clock newHour newMin
    where
        carry   = min `div` 60
        newMin  = min `mod` 60
        newHour = (hour + carry) `mod` 24

toString :: Clock -> String
toString = show

addDelta :: Int -> Int -> Clock -> Clock
addDelta hour min (Clock a b) = Clock newHour newMin
    where
        carry   = (min + b) `div` 60
        newMin  = (min + b) `mod` 60
        newHour = (hour + a + carry) `mod` 24