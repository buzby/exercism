module RunLength (decode, encode) where

decode :: String -> String
decode [] = []
decode n  = replicate len letter ++ decode rest
    where
        (num, letter : rest) = span (\c -> c `elem` ['0'..'9']) n
        len
            | null num  = 1
            | otherwise = read num

encode :: String -> String
encode [] = []
encode n  = count ++ head same : encode rest
    where
        (same, rest) = span (head n==) n
        count
            | length same == 1 = []
            | otherwise        = show $ length same