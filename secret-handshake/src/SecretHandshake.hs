module SecretHandshake (handshake) where

toBinary :: (Integral a, Show a) => a -> String
toBinary 0 = "0"
toBinary 1 = "1"
toBinary n
    | n < 0 = '-' : toBinary (-n)
    | otherwise = toBinary (div n 2) ++ show (n `mod` 2)

handshake :: Int -> [String]
handshake n
    | length binary > 5 = []
    | head padded == '0' = reverse mask
    | otherwise = mask
    where
        binary = toBinary n
        padded = replicate (5 - length binary) '0' ++ binary
        mask = map snd $ filter ((=='1') . fst) $ zip (tail padded) actions
        actions = ["jump", "close your eyes", "double blink", "wink"]