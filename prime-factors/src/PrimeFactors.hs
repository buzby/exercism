module PrimeFactors (primeFactors) where

primeFactors :: Integer -> [Integer]
primeFactors 1 = []
primeFactors n = smallestDivisor : primeFactors (div n smallestDivisor)
    where
        smallestDivisor = head $ filter ((0==) . mod n) [2..]