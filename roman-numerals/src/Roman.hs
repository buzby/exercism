module Roman (numerals) where

numToRoman :: Integral a => a -> String
numToRoman n
    | n >= 1000 = replicate thousands 'M' ++ numToRoman (mod n 1000)
    | n >=  900 =                    "CM" ++ numToRoman (n -    900)
    | n >=  500 =                     'D'  : numToRoman (n -    500)
    | n >=  400 =                    "CD" ++ numToRoman (n -    400)
    | n >=  100 = replicate hundreds  'C' ++ numToRoman (mod n  100)
    | n >=   90 =                    "XC" ++ numToRoman (n -     90)
    | n >=   50 =                     'L'  : numToRoman (n -     50)
    | n >=   40 =                    "XL" ++ numToRoman (n -     40)
    | n >=   10 = replicate tens      'X' ++ numToRoman (mod n   10)
    | n ==    9 =                    "IX"
    | n >=    5 =                     'V'  : numToRoman (n -      5)
    | n ==    4 =                    "IV"
    | n >=    1 = replicate ones      'I'
    | otherwise =                      []
    where
        thousands = fromIntegral $ div n 1000
        hundreds  = fromIntegral $ div n  100
        tens      = fromIntegral $ div n   10
        ones      = fromIntegral       n

numerals :: Integer -> Maybe String
numerals n
    | n < 1     = Nothing
    | otherwise = Just $ numToRoman n