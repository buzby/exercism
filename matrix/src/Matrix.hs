module Matrix
    ( Matrix
    , cols
    , column
    , flatten
    , fromList
    , fromString
    , reshape
    , row
    , rows
    , shape
    , transpose
    ) where

import Data.Vector (Vector)
import qualified Data.Vector as V

type Matrix a = Vector (Vector a)

cols :: Matrix a -> Int
cols m
    | V.null m  = 0
    | otherwise = length $ V.head m

column :: Int -> Matrix a -> Vector a
column n = V.map (V.! (n-1))

flatten :: Matrix a -> Vector a
flatten = V.foldl1 (V.++)

fromList :: [[a]] -> Matrix a
fromList = V.fromList . map V.fromList

fromString :: Read a => String -> Matrix a
fromString [] = V.empty
fromString xs = vals `V.cons` fromString next
    where
        (r, rest) = break (=='\n') xs
        vals        = V.fromList $ map read $ words r
        next
            | null rest = rest
            | otherwise = tail rest

reshape :: (Int, Int) -> Matrix a -> Matrix a
reshape (0, _) _ = V.empty
reshape (1, _) m = V.singleton $ flatten m
reshape (r, c) m = newRow `V.cons` reshape (r-1, c) next
    where
        (newRow, rest) = V.splitAt c $ flatten m
        next           = V.singleton rest

row :: Int -> Matrix a -> Vector a
row n = (V.! (n-1))

rows :: Matrix a -> Int
rows = length

shape :: Matrix a -> (Int, Int)
shape m = (rows m, cols m)

transpose :: Matrix a -> Matrix a
transpose m = V.fromList [vs | c <- [1..cols m], let vs = column c m]
