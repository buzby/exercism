module Meetup (Weekday(..), Schedule(..), meetupDay) where

import Data.Time.Calendar (Day, fromGregorian, dayOfWeek, gregorianMonthLength)

data Weekday = Monday
             | Tuesday
             | Wednesday
             | Thursday
             | Friday
             | Saturday
             | Sunday
             deriving (Enum)

data Schedule = First
              | Second
              | Third
              | Fourth
              | Last
              | Teenth deriving (Eq)

meetupDay :: Schedule -> Weekday -> Integer -> Int -> Day
meetupDay schedule day year month
  | schedule == First = head candidates
  | schedule == Second = head $ drop 1 candidates
  | schedule == Third = head $ drop 2 candidates
  | schedule == Fourth = head $ drop 3 candidates
  | schedule == Last = head $ reverse candidates
  | otherwise = head candidates
  where
    days
      | schedule == Teenth = [13..19]
      | otherwise = [1..gregorianMonthLength year month]
    candidates = [ds | d <- days, let ds = fromGregorian year month d, fromEnum (dayOfWeek ds) == fromEnum day + 1]