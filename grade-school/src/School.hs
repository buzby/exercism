module School (School, add, empty, grade, sorted) where

import Data.List (sort)

type School = [(Int, [String])]

add :: Int -> String -> School -> School
add gradeNum student [] = [(gradeNum, [student])]
add gradeNum student (g : gs)
    | fst g == gradeNum = (gradeNum, sort $ student : snd g) : gs
    | otherwise         = g : add gradeNum student gs 

empty :: School
empty = []

grade :: Int -> School -> [String]
grade _ [] = []
grade gradeNum (g : gs)
    | fst g == gradeNum = snd g 
    | otherwise         = grade gradeNum gs

sorted :: School -> [(Int, [String])]
sorted = sort 