module Brackets (arePaired) where

arePaired :: String -> Bool
arePaired xs = arePaired' xs []
    where
        mirror '('       = ')'
        mirror ')'       = '('
        mirror '['       = ']'
        mirror ']'       = '['
        mirror '{'       = '}'
        mirror '}'       = '{'
        mirror   c       = c
        isOpen   c       = c `elem` "([{"
        isClosed c       = c `elem` ")]}"
        arePaired' [] [] = True
        arePaired' [] _  = False
        arePaired' (y : ys) brackets
            | isOpen y                                = arePaired' ys (y : brackets)
            | isClosed y && null brackets             = False
            | isClosed y && mirror y /= head brackets = False
            | isClosed y                              = arePaired' ys $ tail brackets
            | otherwise                               = arePaired' ys brackets
