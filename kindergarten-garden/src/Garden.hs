module Garden
    ( Plant (..)
    , garden
    , lookupPlants
    ) where

data Plant = Clover
           | Grass
           | Radishes
           | Violets
           deriving (Eq, Show)

type Garden = [(String, [Plant])]

toPlant :: Char -> Plant 
toPlant 'G' = Grass
toPlant 'C' = Clover
toPlant 'R' = Radishes
toPlant 'V' = Violets

garden :: [String] -> String -> Garden
garden [] _ = []
garden (student : students) plantString = (student, plants) : garden students rest
    where
        rows   =   words                       plantString
        plants =  concat $ map (map toPlant . take 2) rows
        rest   = unwords $ map (              drop 2) rows

lookupPlants :: String -> Garden -> [Plant]
lookupPlants _ [] = []
lookupPlants student (g : gs)
    | student == fst g =                  snd g 
    | otherwise        = lookupPlants student gs 