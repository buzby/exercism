module Atbash (decode, encode) where

plainToCipher :: Char -> Char
plainToCipher c 
    | c `elem` ['a'..'z'] = toEnum $ 219 - fromEnum c
    | c `elem` ['A'..'Z'] = toEnum $ 187 - fromEnum c
    | otherwise           = c

separate :: String -> String
separate xs
    | null rest = word
    | otherwise = word ++ ' ' : separate rest
    where
        (word, rest) = splitAt 5 xs

decode :: String -> String
decode = map plainToCipher . filter (\c -> c `elem` (['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']))

encode :: String -> String
encode = separate . decode
