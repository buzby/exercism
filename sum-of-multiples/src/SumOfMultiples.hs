module SumOfMultiples (sumOfMultiples) where

sumOfMultiples :: Integral a => [a] -> a -> a
sumOfMultiples [] _ = 0
sumOfMultiples factors limit
    | 0 `elem` factors = sumOfMultiples (filter (/= 0) factors) limit
    | otherwise        = sum [n | n <- [1..limit-1], not $ all ((/=0) . mod n) factors]
