module Bob (responseFor) where

removeWhitespace :: String -> String
removeWhitespace = concat . words

removePunctuation :: String -> String
removePunctuation = filter (\x -> elem x alphanum)
    where
        alphanum = ['0'..'9'] ++ ['a'..'z'] ++ ['A'..'Z']

leaveLowers :: String -> String
leaveLowers = filter (\x -> elem x ['a'..'z'])

leaveUppers :: String -> String
leaveUppers = filter (\x -> elem x ['A'..'Z'])

responseFor :: String -> String
responseFor [] = "Fine. Be that way!"
responseFor xs
    | null noWhitespace = "Fine. Be that way!"
    | isQuestion && isYelling = "Calm down, I know what I'm doing!"
    | isQuestion = "Sure."
    | isYelling = "Whoa, chill out!"
    | isSilent = "Fine. Be that way!"
    | otherwise = "Whatever."
    where
        noWhitespace = removeWhitespace xs
        isQuestion = (== '?') $ head $ reverse noWhitespace
        noPunctuation = removePunctuation noWhitespace
        isSilent = null noPunctuation
        lowers = leaveLowers noPunctuation
        uppers = leaveUppers noPunctuation
        isYelling = null lowers && (not $ null uppers)