module Triangle (rows) where

factorial :: Integral a => a -> a
factorial n = product [1..n]

choose :: Integral a => a -> a -> a
choose n r = div (factorial n) (factorial r * factorial (n - r))

rows :: Int -> [[Integer]]
rows 0 = []
rows x = rows (fromIntegral n) ++ [map (choose n) [0..n]]
    where
        n = fromIntegral $ x - 1