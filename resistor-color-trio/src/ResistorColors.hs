module ResistorColors (Color(..), Resistor(..), label, ohms) where

data Color =
    Black
  | Brown
  | Red
  | Orange
  | Yellow
  | Green
  | Blue
  | Violet
  | Grey
  | White
  deriving (Show, Enum, Bounded)

newtype Resistor = Resistor { bands :: (Color, Color, Color) }
  deriving Show

value :: Color -> Int 
value Black  = 0
value Brown  = 1
value Red    = 2
value Orange = 3
value Yellow = 4
value Green  = 5
value Blue   = 6
value Violet = 7
value Grey   = 8
value White  = 9

label :: Resistor -> String
label resistor
  | zeroes < 3 =           resistance ++     " ohms"
  | zeroes < 6 = dropEnd 3 resistance ++ " kiloohms"
  | zeroes < 9 = dropEnd 6 resistance ++ " megaohms"
  | otherwise  = dropEnd 9 resistance ++ " gigaohms"
  where
    resistance   = show   $ ohms resistor
    zeroes       = length $ filter (=='0') resistance
    dropEnd n xs = take (length xs - n) xs

ohms :: Resistor -> Int
ohms resistor = (10 * value a + value b) * 10 ^ value c 
  where
    (a, b, c) = bands resistor