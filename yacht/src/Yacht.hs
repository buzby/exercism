module Yacht (yacht, Category(..)) where

import qualified Data.List as L

data Category = Ones
              | Twos
              | Threes
              | Fours
              | Fives
              | Sixes
              | FullHouse
              | FourOfAKind
              | LittleStraight
              | BigStraight
              | Choice
              | Yacht

yacht :: Category -> [Int] -> Int
yacht _ [] = 0
yacht _ xs
       | length xs /= 5 = 0
yacht Ones           xs = sum $ filter (==1) xs
yacht Twos           xs = sum $ filter (==2) xs
yacht Threes         xs = sum $ filter (==3) xs
yacht Fours          xs = sum $ filter (==4) xs
yacht Fives          xs = sum $ filter (==5) xs
yacht Sixes          xs = sum $ filter (==6) xs
yacht FullHouse      xs
       | length groups == 2 && elem (length $ head $ groups) [2,3] = sum xs
       | otherwise                                                 = 0
       where
              groups = L.group $ L.sort xs
yacht FourOfAKind    xs
       | not $ null fours = sum $ take 4 $ head $ fours
       | otherwise        = 0
       where
              groups = L.group $ L.sort xs
              fours  = filter ((>=4) . length) groups
yacht LittleStraight xs
       | L.sort xs == [1..5] = 30
       | otherwise           =  0
yacht BigStraight    xs
       | L.sort xs == [2..6] = 30
       | otherwise           =  0
yacht Choice         xs = sum xs
yacht Yacht          xs
       | length groups == 1  = 50
       | otherwise           =  0
       where
              groups = L.group $ L.sort xs