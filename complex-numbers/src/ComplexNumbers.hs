module ComplexNumbers
(Complex,
 conjugate,
 abs,
 exp,
 real,
 imaginary,
 mul,
 add,
 sub,
 div,
 complex) where

import Prelude hiding (abs)

-- Data definition -------------------------------------------------------------
data Complex a = Complex (a, a) deriving(Eq, Ord)

instance (Real a, Show a) => Show (Complex a) where
    show (Complex (a, b))
        | a == 0    = show b ++ "i"
        | b == 0    = show a
        | b >  0    = show a ++ " + " ++ show   b  ++ "i"
        | otherwise = show a ++ " - " ++ show (-b) ++ "i"

instance Floating a => Num (Complex a) where
    fromInteger n = Complex (fromInteger n, 0)
    (+) (Complex (a, b)) (Complex (c, d)) = Complex (a+c, b+d)
    (-) (Complex (a, b)) (Complex (c, d)) = Complex (a-c, b-d)
    (*) (Complex (a, b)) (Complex (c, d)) = Complex (a*c - b*d, b*c + a*d)

instance Floating a => Fractional (Complex a) where
    fromRational x = Complex (fromRational x, 0)
    recip (Complex (a, b)) = Complex (a/(a^2 + b^2), (-b)/(a^2 + b^2))

instance Floating a => Floating (Complex a) where
    exp (Complex (a, b)) = Complex (exp a, 0) * Complex (cos b, sin b)

instance (Floating a, Ord a) => Real (Complex a)
instance Enum (Complex a)

instance (Floating a, Ord a) => Integral (Complex a) where
    div = (/)

complex :: Real a => (a, a) -> Complex a
complex (a, b) = Complex (a, b)

-- unary operators -------------------------------------------------------------
conjugate :: Num a => Complex a -> Complex a
conjugate (Complex (a, b)) = Complex (a, (-b))

real :: Num a => Complex a -> a
real (Complex (a, b)) = a

imaginary :: Num a => Complex a -> a
imaginary (Complex (a, b)) = b 

abs :: Floating a => Complex a -> a 
abs (Complex (a, b)) = sqrt $ a^2 + b^2

-- binary operators ------------------------------------------------------------
mul :: Floating a => Complex a -> Complex a -> Complex a
mul = (*)

add :: Floating a => Complex a -> Complex a -> Complex a
add = (+)

sub :: Floating a => Complex a -> Complex a -> Complex a
sub = (-)