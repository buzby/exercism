module Hamming (distance) where

distance :: Integral a => String -> String -> Maybe a
distance xs ys
    | length xs - length ys /= 0 = Nothing
    | otherwise = Just $ fromIntegral $ length $ filter (not . id) $ zipWith (==) xs ys