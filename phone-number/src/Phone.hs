module Phone (number) where

number :: String -> Maybe String
number [] = Nothing
number xs
    | length     clean /= 10 = Nothing
    | not $ isN (clean !! 0) = Nothing
    | not $ isN (clean !! 3) = Nothing
    | otherwise              = Just clean
    where
        isN c = c `elem` ['2'..'9']
        isX c = c `elem` ['0'..'9']
        clean = dropWhile (=='1') $ filter isX xs