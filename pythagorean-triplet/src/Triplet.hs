module Triplet (tripletsWithSum) where

import Data.List (nub)

maxM :: Int -> Int -> Int
maxM total k = ceiling $ ((sqrt $ fromIntegral $ div (total * 2) k + 1) - 1) / 2

tripletsWithSum :: Int -> [(Int, Int, Int)]
tripletsWithSum total
    | odd total = []
    | otherwise = nub [(a, b, c) | k <- [1..div total 2], m <- [2..maxM total k], n <- [1..m - 1], total `mod` (2*k) == 0, m*(m+n) == (div total (2*k)),
                   let a = min (k * (m^2 - n^2)) (2*k*m*n), let b = max (k * (m^2 - n^2)) (2*k*m*n), let c = k * (m^2 + n^2)]