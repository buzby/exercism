module ProteinTranslation(proteins) where

proteins :: String -> Maybe [String]
proteins [] = Just []
proteins rna
    | length rna < 3                            =                                  Nothing
    | codon ==      "AUG"                       = fmap ("Methionine"    :) $ proteins rest
    | codon `elem` ["UUU", "UUC"]               = fmap ("Phenylalanine" :) $ proteins rest
    | codon `elem` ["UUA", "UUG"]               = fmap ("Leucine"       :) $ proteins rest
    | codon `elem` ["UCU", "UCC", "UCA", "UCG"] = fmap ("Serine"        :) $ proteins rest
    | codon `elem` ["UAU", "UAC"]               = fmap ("Tyrosine"      :) $ proteins rest
    | codon `elem` ["UGU", "UGC"]               = fmap ("Cysteine"      :) $ proteins rest
    | codon ==      "UGG"                       = fmap ("Tryptophan"    :) $ proteins rest
    | codon `elem` ["UAA", "UAG", "UGA"]        =                                  Just []
    | otherwise                                 =                                  Nothing
    where
        (codon, rest) = splitAt 3 rna