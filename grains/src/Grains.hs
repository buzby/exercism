module Grains (square, total) where

square :: Integral a => a -> Maybe a
square n
    | n < 1 || n > 64 = Nothing
    | otherwise = Just $ 2^(n-1) 

total :: Integral a => a
total = 2^64 - 1
