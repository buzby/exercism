module Beer (song) where

import Data.Char (toUpper)

getVerse :: Int -> String
getVerse n = capitalise bottles     ++ " bottle" ++     plural ++ "of beer on the wall, "  ++ 
                        bottles     ++ " bottle" ++     plural ++ "of beer.\n"             ++
                        takeOrGo    ++ 
                        nextBottles ++ " bottle" ++ nextPlural ++ "of beer on the wall.\n" ++ endVerse
       where
             capitalise (x : xs) = toUpper x : xs
             bottles
                 | n == 0    = "no more"
                 | otherwise =    show n 
             plural
                 | n == 1    =  " "
                 | otherwise = "s "
             itOrOne
                 | n == 1    =  "it"
                 | otherwise = "one"
             takeOrGo
                 | n == 0    =              "Go to the store and buy some more, "
                 | otherwise = "Take " ++ itOrOne ++ " down and pass it around, "
             nextBottles
                 | n == 1    =    "no more"
                 | n == 0    =         "99"
                 | otherwise = show $ n - 1
             nextPlural
                 | n == 2    =  " "
                 | otherwise = "s "
             endVerse
                 | n == 0    =   ""
                 | otherwise = "\n"

song :: String
song = concat $ map getVerse [99,98..0]