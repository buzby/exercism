module Diamond (diamond) where

getWidth :: Char -> Int
getWidth c = snd $ head [(letter, width) | n <- [0..25], let letter = ['A'..'Z'] !! n, let width = 2*n + 1, letter == c]

makeRow :: Char -> Char -> String
makeRow maxChar c 
    | c == 'A' = margin ++ c : margin
    | otherwise = margin ++ c : middle ++ c : margin
    where
        margin = replicate (div (getWidth maxChar - getWidth c) 2) ' '
        middle = replicate (getWidth c - 2) ' '

diamond :: Char -> Maybe [String]
diamond c 
    | not $ c `elem` ['A'..'Z'] = Nothing
    | otherwise                 = Just $ map (makeRow c) $ ['A'..c] ++ (tail $ reverse ['A'..c])