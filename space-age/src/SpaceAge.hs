module SpaceAge (Planet(..), ageOn) where

data Planet = Mercury
            | Venus
            | Earth
            | Mars
            | Jupiter
            | Saturn
            | Uranus
            | Neptune 

secsToYears :: Floating a => a -> a 
secsToYears = (/ 31557600)

earthToPlanet :: Floating a => Planet -> a -> a
earthToPlanet Mercury = (/ 0.2408467)
earthToPlanet Venus = (/ 0.61519726)
earthToPlanet Earth = id
earthToPlanet Mars = (/ 1.8808158)
earthToPlanet Jupiter = (/ 11.862615)
earthToPlanet Saturn = (/ 29.447498)
earthToPlanet Uranus = (/ 84.016846)
earthToPlanet Neptune = (/ 164.79132)

ageOn :: Floating a => Planet -> a -> a
ageOn = (secsToYears .) . earthToPlanet