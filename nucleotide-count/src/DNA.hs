module DNA (nucleotideCounts, Nucleotide(..)) where

import Data.Map (Map, fromList, adjust)

data Nucleotide = A | C | G | T deriving (Eq, Ord, Show)

nucleotideCounts :: String -> Either String (Map Nucleotide Int)
nucleotideCounts [] = Right $ fromList [(A, 0), (C, 0), (G, 0), (T, 0)]
nucleotideCounts (x : xs)
    | x == 'A' = fmap (adjust (+1) A) $ nucleotideCounts xs
    | x == 'C' = fmap (adjust (+1) C) $ nucleotideCounts xs
    | x == 'G' = fmap (adjust (+1) G) $ nucleotideCounts xs
    | x == 'T' = fmap (adjust (+1) T) $ nucleotideCounts xs
    | otherwise = Left []