module LinkedList
    ( LinkedList
    , datum
    , fromList
    , isNil
    , new
    , next
    , nil
    , reverseLinkedList
    , toList
    ) where

data LinkedList a = LinkedList { datum :: a
                               , next  :: LinkedList a
                               }
                               | Empty 
                               deriving (Eq, Show)

fromList :: [a] -> LinkedList a
fromList []       = Empty
fromList (x : xs) = LinkedList {datum = x, next = fromList xs}

isNil :: LinkedList a -> Bool
isNil Empty = True
isNil _     = False 

new :: a -> LinkedList a -> LinkedList a
new x linkedList = LinkedList {datum = x, next = linkedList}

nil :: LinkedList a
nil = Empty

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList linkedList = rev linkedList Empty
    where
        rev Empty a = a
        rev l     a = rev (next l) (new (datum l) a)

toList :: LinkedList a -> [a]
toList Empty = []
toList linkedList = datum linkedList : toList (next linkedList)