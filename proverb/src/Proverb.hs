module Proverb(recite) where

a :: String -> String
a []       = []
a (x : _)
    | x `elem` ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'] = "an "
    | otherwise                                                   = "a "

forWant :: [String] -> String
forWant []             = []
forWant [_]            = []
forWant (x1 : x2 : xs) = "For want of " ++ a x1 ++ x1 ++ " the " ++ x2 ++ " was lost.\n" ++ forWant (x2 : xs)

recite :: [String] -> String
recite []       = []
recite (x : xs) = forWant (x : xs) ++ "And all for the want of " ++ a x ++ x ++ "."