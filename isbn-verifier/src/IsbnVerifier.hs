module IsbnVerifier (isbn) where

digitToInt :: Char -> Int 
digitToInt c 
    | not $ c `elem` 'X' : ['0'..'9'] = error "Not a digit"
    | otherwise                       = snd $ head $ filter ((==c) . fst) $ zip ('X' : ['0'..'9']) (10 : [0..9])

isbn :: String -> Bool
isbn [] = False
isbn xs
    | length clean /= 10                                                 = False
    | not $ null $ filter (\x -> not $ x `elem` ['0'..'9']) $ init clean = False
    | mod (sum $ zipWith (*) [10,9..1] $ map digitToInt clean) 11 == 0   = True
    | otherwise                                                          = False
    where
        clean = filter (\x -> x `elem` 'X' : ['0'..'9']) xs