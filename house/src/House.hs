module House (rhyme) where

import Data.List (intersperse)

verse :: Int -> String
verse 1 = "This is the house that Jack built.\n"
verse n = thisis ++ addition n ++ rest
        where
                (thisis, rest) = splitAt 7 $ verse $ n - 1
                addition 2     = " the malt\nthat lay in"
                addition 3     = " the rat\nthat ate"
                addition 4     = " the cat\nthat killed"
                addition 5     = " the dog\nthat worried"
                addition 6     = " the cow with the crumpled horn\nthat tossed"
                addition 7     = " the maiden all forlorn\nthat milked"
                addition 8     = " the man all tattered and torn\nthat kissed"
                addition 9     = " the priest all shaven and shorn\nthat married"
                addition 10    = " the rooster that crowed in the morn\nthat woke"
                addition 11    = " the farmer sowing his corn\nthat kept"
                addition 12    = " the horse and the hound and the horn\nthat belonged to"

rhyme :: String
rhyme = concat $ intersperse "\n" $ map verse [1..12]