module Series (Error(..), largestProduct) where

data Error = InvalidSpan | InvalidDigit Char deriving (Show, Eq)

fromDigit :: (Integral a, Read a) => Char -> a
fromDigit = read . (:[])

maxProduct :: (Integral a, Integral b, Read b) => a -> String -> b
maxProduct len digits
    | fromIntegral len > length digits = 0
    | otherwise                        = max (product $ map fromDigit $ take (fromIntegral len) digits) (maxProduct len $ tail digits)

largestProduct :: Int -> String -> Either Error Integer
largestProduct 0 _ = Right 1
largestProduct size digits
    | size > length digits || size < 0 = Left InvalidSpan
    | not $ null invalid               = Left $ InvalidDigit $ head invalid
    | otherwise                        = Right $ maxProduct size digits
    where
        invalid = filter (\c -> not $ c `elem` ['0'..'9']) digits