module Base (Error(..), rebase) where

data Error a = InvalidInputBase | InvalidOutputBase | InvalidDigit a
    deriving (Show, Eq)

fromDigits :: Integral a => [a] -> a -> a
fromDigits digits base = sum $ zipWith (\a b -> a * base^b) digits [length digits - 1, length digits - 2..0]

toDigits :: Integral a => a -> a -> [a]
toDigits 0 _    = []
toDigits n base = toDigits (div n base) base ++ [n `mod` base]

rebase :: Integral a => a -> a -> [a] -> Either (Error a) [a]
rebase inputBase outputBase inputDigits
    |  inputBase < 2     = Left InvalidInputBase
    | outputBase < 2     = Left InvalidOutputBase
    | null inputDigits   = Right []
    | not $ null invalid = Left  $ InvalidDigit $ head invalid
    | otherwise          = Right $ toDigits (fromDigits inputDigits inputBase) outputBase
    where
        invalid = filter (\d -> not $ d `elem` [0..inputBase - 1]) inputDigits