module TwelveDays (recite) where

recite :: Int -> Int -> [String]
recite start stop 
    | start > stop = []
    | stop > 12    = []
    | start < 1    = []
    | otherwise    = ("On the " ++ cardinal start ++ " day of Christmas my true love gave to me: " ++ day start) : recite (start+1) stop
    where
        cardinal  1 = "first"
        cardinal  2 = "second"
        cardinal  3 = "third"
        cardinal  4 = "fourth"
        cardinal  5 = "fifth"
        cardinal  6 = "sixth"
        cardinal  7 = "seventh"
        cardinal  8 = "eighth"
        cardinal  9 = "ninth"
        cardinal 10 = "tenth"
        cardinal 11 = "eleventh"
        cardinal 12 = "twelfth"
        day       1 = "a Partridge in a Pear Tree."
        day       2 = "two Turtle Doves, and "     ++ day  1
        day       3 = "three French Hens, "        ++ day  2
        day       4 = "four Calling Birds, "       ++ day  3
        day       5 = "five Gold Rings, "          ++ day  4
        day       6 = "six Geese-a-Laying, "       ++ day  5
        day       7 = "seven Swans-a-Swimming, "   ++ day  6
        day       8 = "eight Maids-a-Milking, "    ++ day  7
        day       9 = "nine Ladies Dancing, "      ++ day  8
        day      10 = "ten Lords-a-Leaping, "      ++ day  9
        day      11 = "eleven Pipers Piping, "     ++ day 10
        day      12 = "twelve Drummers Drumming, " ++ day 11