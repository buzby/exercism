{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_raindrops (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [1,1,0,6] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/edmondbuzby/Exercism/haskell/raindrops/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/bin"
libdir     = "/Users/edmondbuzby/Exercism/haskell/raindrops/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/lib/x86_64-osx-ghc-8.8.3/raindrops-1.1.0.6-ETCBqlJahVaBjgilbIawdU-test"
dynlibdir  = "/Users/edmondbuzby/Exercism/haskell/raindrops/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/lib/x86_64-osx-ghc-8.8.3"
datadir    = "/Users/edmondbuzby/Exercism/haskell/raindrops/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/share/x86_64-osx-ghc-8.8.3/raindrops-1.1.0.6"
libexecdir = "/Users/edmondbuzby/Exercism/haskell/raindrops/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/libexec/x86_64-osx-ghc-8.8.3/raindrops-1.1.0.6"
sysconfdir = "/Users/edmondbuzby/Exercism/haskell/raindrops/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "raindrops_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "raindrops_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "raindrops_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "raindrops_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "raindrops_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "raindrops_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
