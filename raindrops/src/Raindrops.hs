module Raindrops (convert) where

maskFilter :: [Bool] -> [a] -> [a]
maskFilter mask = map snd . filter fst . zip mask

convert :: (Integral a, Show a) => a -> String
convert n
    | and $ map not mask = show n 
    | otherwise          = concat $ maskFilter mask raindrops 
    where
        mask      = map ((==0) . mod n) [3, 5, 7]
        raindrops = ["Pling", "Plang", "Plong"]