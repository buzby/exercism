module Isogram (isIsogram) where

toLower :: Char -> Char
toLower c 
    | c `elem` ['A'..'Z'] = snd $ head $ filter ((c==) . fst) $ zip ['A'..'Z'] ['a'..'z']
    | otherwise           = c

isLetter :: Char -> Bool
isLetter c = toLower c `elem` ['a'..'z']

isIsogram :: String -> Bool
isIsogram [] = True
isIsogram (c : cs)
    | not $ isLetter c                     = isIsogram cs 
    | toLower c `elem` map toLower cs      = False
    | otherwise                            = isIsogram cs 