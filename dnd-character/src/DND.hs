module DND ( Character(..)
           , ability
           , modifier
           , character
           ) where

import Test.QuickCheck (Gen, choose, generate, vectorOf)
import Data.List (sort)

data Character = Character
  { strength     :: Int
  , dexterity    :: Int
  , constitution :: Int
  , intelligence :: Int
  , wisdom       :: Int
  , charisma     :: Int
  , hitpoints    :: Int
  }
  deriving (Show, Eq)

modifier :: Int -> Int
modifier n = div (n - 10) 2

ability :: Gen Int
ability = fmap (sum . tail . sort) $ vectorOf 4 $ choose (1, 6)

character :: Gen Character
character = do
  str <- ability
  dex <- ability
  con <- ability
  int <- ability
  wis <- ability
  cha <- ability
  let hit = 10 + modifier con
  return Character {strength=str, dexterity=dex, constitution=con, intelligence=int, wisdom=wis, charisma=cha, hitpoints=hit}