{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_accumulate (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,3] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/edmondbuzby/Exercism/haskell/accumulate/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/bin"
libdir     = "/Users/edmondbuzby/Exercism/haskell/accumulate/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/lib/x86_64-osx-ghc-8.8.3/accumulate-0.1.0.3-B4PfkiTCIglIOJc3UnLm9b"
dynlibdir  = "/Users/edmondbuzby/Exercism/haskell/accumulate/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/lib/x86_64-osx-ghc-8.8.3"
datadir    = "/Users/edmondbuzby/Exercism/haskell/accumulate/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/share/x86_64-osx-ghc-8.8.3/accumulate-0.1.0.3"
libexecdir = "/Users/edmondbuzby/Exercism/haskell/accumulate/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/libexec/x86_64-osx-ghc-8.8.3/accumulate-0.1.0.3"
sysconfdir = "/Users/edmondbuzby/Exercism/haskell/accumulate/.stack-work/install/x86_64-osx/1cfc5bece9df1c1f6ee9c02fe6fca196b3b70f9a65866095e0b9cae5fb6a1a49/8.8.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "accumulate_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "accumulate_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "accumulate_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "accumulate_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "accumulate_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "accumulate_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
