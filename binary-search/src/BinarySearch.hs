module BinarySearch (find) where

import Data.Array

find :: Ord a => Array Int a -> a -> Maybe Int
find arr x
    | null $ elems arr  = Nothing
    | arr ! middle == x = Just middle
    | arr ! middle >  x = find (ixmap (start, middle - 1) id arr) x
    | arr ! middle <  x = find (ixmap (middle + 1, end) id arr) x
    | otherwise         = Nothing
    where
        (start, end) = bounds arr
        middle       = div (start + end) 2