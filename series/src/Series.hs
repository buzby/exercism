module Series (slices) where

import Data.Char (digitToInt)

slices :: Int -> String -> [[Int]]
slices 0 [] = [[]]
slices _ [] =  []
slices n xs
    | n > length xs = []
    | otherwise     = (map digitToInt $ take n xs) : (slices n $ tail xs)