module ETL (transform) where

import qualified Data.Map as M
import           Data.Map (Map)

toLower :: Char -> Char
toLower c 
    | c `elem` ['A'..'Z'] = snd $ head $ filter ((==c) . fst) $ zip ['A'..'Z'] ['a'..'z']
    | otherwise           = c 

transform :: Ord a => Map a String -> Map Char a
transform legacyData = M.fromList [(toLower letter, points) | points <- M.keys legacyData, letter <- (\(Just x) -> x) $ M.lookup points legacyData]