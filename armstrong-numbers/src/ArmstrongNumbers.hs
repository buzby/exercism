module ArmstrongNumbers (armstrong) where

import Data.Char (digitToInt)

armstrong :: (Integral a, Show a) => a -> Bool
armstrong n = n == (sum $ map (^pow) digits)
    where
        digits = map (fromIntegral . digitToInt) $ show n 
        pow = length digits