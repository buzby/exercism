module Acronym (abbreviate) where

import qualified Data.Text as T 
import           Data.Text (Text)

toWords :: Text -> [Text]
toWords = T.split (\x -> x `elem` " -")

allCaps :: Text -> Bool
allCaps = T.null . T.filter (\c -> not $ c `elem` ['A'..'Z'])

camelCase :: Text -> Text
camelCase word
    | T.null       word                                   = T.empty
    | not $ T.head word `elem` (['a'..'z'] ++ ['A'..'Z']) = camelCase $ T.tail word
    | otherwise                                           = T.toUpper $ T.head word `T.cons` (T.filter (\c -> c `elem` ['A'..'Z']) $ T.tail word)

makeAcronym :: [Text] -> Text
makeAcronym [] = T.empty
makeAcronym (word : rest)
    | T.null  word = makeAcronym rest
    | allCaps word = T.head word `T.cons` makeAcronym rest
    | otherwise    = camelCase word `T.append` makeAcronym rest

abbreviate :: String -> String
abbreviate = T.unpack . makeAcronym . toWords . T.pack