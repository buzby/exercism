module Pangram (isPangram) where

import Data.Char (toLower)
import Data.List (intersect)

isPangram :: String -> Bool
isPangram = (== alphabet) . intersect alphabet . map toLower
    where
        alphabet = ['a'..'z']
