module Robot
    ( Bearing(East,North,South,West)
    , bearing
    , coordinates
    , mkRobot
    , move
    ) where

data Bearing = North
             | East
             | South
             | West
             deriving (Eq, Show)

instance Enum Bearing where
    toEnum 0 = North
    toEnum 1 = East
    toEnum 2 = South
    toEnum 3 = West
    toEnum n = toEnum $ n `mod` 4
    fromEnum North = 0
    fromEnum East = 1
    fromEnum South = 2
    fromEnum West = 3

data Robot = Robot { bearing :: Bearing
                   , coordinates :: (Integer, Integer)
                   } deriving (Show)

mkRobot :: Bearing -> (Integer, Integer) -> Robot
mkRobot direction coords = Robot {bearing=direction, coordinates=coords}

move :: Robot -> String -> Robot
move robot [] = robot 
move robot (c : cs) = move (mkRobot direction coords) cs
    where
        (x, y) = coordinates robot
        direction
            | c == 'R' = succ $ bearing robot 
            | c == 'L' = pred $ bearing robot 
            | otherwise = bearing robot 
        coords
            | c /= 'A' = (x, y)
            | direction == North = (x, y+1)
            | direction == East = (x+1, y)
            | direction == South = (x, y-1)
            | direction == West = (x-1, y)
            | otherwise = (x, y)