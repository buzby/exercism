module Matrix (saddlePoints) where

import Data.Array

getRow :: (Enum i, Ix i) => Array (i, i) e -> i -> [e]
getRow m r = [m ! (r, c) | c <- [start..end]]
    where
        ((_, start), (_, end)) = bounds m

getColumn :: (Enum i, Ix i) => Array (i, i) e -> i -> [e]
getColumn m c = [m ! (r, c) | r <- [start..end]]
    where
        ((start, _), (end, _)) = bounds m

isSaddlePoint :: (Enum i, Ix i, Ord e) => Array (i, i) e -> (i, i) -> Bool
isSaddlePoint m (r, c) = (value == maximum row) && (value == minimum column)
    where
        value  = m ! (r, c)
        row    = getRow    m r
        column = getColumn m c

saddlePoints :: (Enum i, Ix i, Ord e) => Array (i, i) e -> [(i, i)]
saddlePoints m = filter (isSaddlePoint m) $ indices m