module Allergies (Allergen(..), allergies, isAllergicTo) where

data Allergen = Eggs
              | Peanuts
              | Shellfish
              | Strawberries
              | Tomatoes
              | Chocolate
              | Pollen
              | Cats
              deriving (Eq, Show, Enum)

allergies :: Int -> [Allergen]
allergies 0 = []
allergies score
    | allergenEnum > 7 =  allergies $ score - 2 ^ allergenEnum
    | otherwise        = (allergies $ score - 2 ^ allergenEnum) ++ [toEnum allergenEnum]
    where
        allergenEnum = floor $ log (fromIntegral score) / log 2

isAllergicTo :: Allergen -> Int -> Bool
isAllergicTo allergen score = allergen `elem` allergies score
