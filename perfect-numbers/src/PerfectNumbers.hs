module PerfectNumbers (classify, Classification(..)) where

data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)

getFactors :: Integral a => a -> [a]
getFactors 1 = []
getFactors n
    | isSquare  = firstHalf ++ tail secondHalf
    | otherwise = firstHalf ++ secondHalf
    where
        root       = floor $ sqrt $ fromIntegral n 
        isSquare   = root^2 == n
        firstHalf  = filter (\x -> mod n x == 0) [1..root]
        secondHalf = map (div n) $ reverse $ tail firstHalf

classify :: Integral a => a -> Maybe Classification
classify n 
    | n <  1                    = Nothing
    | n == (sum $ getFactors n) = Just Perfect 
    | n <  (sum $ getFactors n) = Just Abundant
    | otherwise                 = Just Deficient
