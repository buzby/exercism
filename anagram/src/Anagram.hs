module Anagram (anagramsFor) where

import Data.List (sort)
import Data.Char (toLower)

anagramsFor :: String -> [String] -> [String]
anagramsFor [] _ = []
anagramsFor _ [] = []
anagramsFor word (x : xs)
    |       map toLower word  ==       map toLower x  =     anagramsFor word xs
    | sort (map toLower word) == sort (map toLower x) = x : anagramsFor word xs
    | otherwise                                       =     anagramsFor word xs